{ pkgs, ... }:

{
# Home Manager needs a bit of information about you and the
# paths it should manage.
home.username = "desktop";
home.homeDirectory = "/home/desktop";

# This value determines the Home Manager release that your
# configuration is compatible with. This helps avoid breakage
# when a new Home Manager release introduces backwards
# incompatible changes.
#
# You can update Home Manager without changing this value. See
# the Home Manager release notes for a list of state version
# changes in each release.
home.stateVersion = "24.11";

# Overlays
nixpkgs.overlays = [
    (self: super: {
        mangal = super.mangal.overrideAttrs (old: {
            src = super.fetchFromGitHub {
                owner = "sergiolaverde0";
                repo = "mangal";
                rev = "v4.0.7";
                hash = "sha256-M8xbozvVqn4x3Wd9cuSETFQ6PVrHHlJUIZOryDlNAhc=";
            };
            });
     })
];

# Let Home Manager install and manage itself.
programs.home-manager.enable = true;

# Enable and configure things, not really sure which ones
programs.ncmpcpp = {
    enable = true;
    package = pkgs.ncmpcpp.override { visualizerSupport = true; };
};

#Packages to install automatically
home.packages = with pkgs; [
    atuin
    basedpyright
    clang-tools
    cppcheck
    delta
    dua
    glow
    gopls
    gtrash
    hadolint
    harper
    html-tidy
    kopia
    lazydocker
    lazygit
    lf
    lsd
    lua-language-server
    mangal
    mypy
    nil
    nodejs_20
    nodePackages.bash-language-server
    nodePackages.dockerfile-language-server-nodejs
    nodePackages.eslint
    nodePackages.neovim
    nodePackages.prettier
    python312Packages.python-lsp-server
    qownnotes
    ruff
    selene
    so
    taplo
    typescript-language-server
    uv
    vscode-langservers-extracted
    zsh-autosuggestions
    zsh-syntax-highlighting
];
# Services
services.mpdris2 ={
    enable = true;
    mpd.host = "127.0.0.1";
    mpd.musicDirectory = ~/Music;
    mpd.port = 6600;
    notifications = false;
    };
services.listenbrainz-mpd ={
    enable = true;
    settings = {
        submission = {
            token_file = "/home/desktop/Secrets/listenbrainz-token";
            };
        mpd = {
            address = "127.0.0.1:6600";
            };
        };
    };

nix = {
    package = pkgs.nix;
    settings = {
        experimental-features = [ "nix-command" "flakes" ];
        use-xdg-base-directories = true;
    };
};

# Make this work out of NixOS (actually does nothing).
targets.genericLinux.enable = true;
nixpkgs.config.allowUnfree = true;
}
