local _M = {}

-- Associative Array (Hash)

_M.color = {
    ["bgnormal"] = "#ffffff", -- white
    ["fgnormal"] = "#000000", -- black
    ["bgfocus"] = "#f5f5f5", -- grey100
    ["fgdim"] = "#9e9e9e", -- grey500
    ["red"] = "#f44336", -- red500
    ["magenta"] = "#e91e63", -- pink500
    ["blue"] = "#2196f3", --blue500
    ["yellow"] = "#ffeb3b", -- yellow500
    ["cyan"] = "#009688", -- teal500
    ["green"] = "#4caf50", -- green500
    ["orange"] = "#ff9800", --orange500
}

return _M
