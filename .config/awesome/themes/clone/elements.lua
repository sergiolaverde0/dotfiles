local palette = require("themes.clone.palette")

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

theme.font          = "FiraCode 9"
theme.taglist_font  = "FiraCode Nerd Font Mono 9"

theme.bg_normal     = palette.color['bgnormal']     .. "cc"
theme.bg_focus      = palette.color['red']    .. "cc"
theme.bg_urgent     = palette.color['orange'] .. "cc"
theme.bg_minimize   = palette.color['fgdim']   .. "cc"
theme.bg_systray    = palette.color['bgfocus']   .. "cc"

theme.fg_normal     = palette.color['fgnormal']
theme.fg_focus      = palette.color['bgnormal']
theme.fg_urgent     = palette.color['bgnormal']
theme.fg_minimize   = palette.color['bgnormal']

theme.useless_gap   = dpi(20)
theme.border_width  = dpi(7)

theme.border_normal = palette.color['blue']   .. "cc"
theme.border_focus  = palette.color['red']    .. "cc"
theme.border_marked = palette.color['orange'] .. "cc"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:

theme.taglist_bg_focus = palette.color['red'] .. "cc"
--theme.taglist_bg_focus = "png:" .. theme_path .. "misc/copycat-holo/taglist_bg_focus.png"
theme.taglist_fg_focus = palette.color['bgnormal']


theme.tasklist_bg_normal = palette.color['bgnormal']    .. "88"
--theme.tasklist_bg_normal = "png:" .. theme_path .. "misc/copycat-holo/bg_focus.png"
theme.tasklist_bg_focus  = palette.color['red']   .. "88"
--theme.tasklist_bg_focus  = "png:" .. theme_path .. "misc/copycat-holo/bg_focus_noline.png"
theme.tasklist_fg_focus  = palette.color['fgnormal']

theme.titlebar_bg_normal = palette.color['bgnormal']   .. "cc"
theme.titlebar_bg_focus  = palette.color['bgnormal']   .. "cc"
theme.titlebar_fg_focus  = palette.color['fgnormal']   .. "cc"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, palette.color['fgnormal']
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, palette.color['bgnormal']
)

-- Display the taglist squares

-- override
theme.taglist_squares_sel      = theme_path .. "taglist/clone/square_sel.png"
theme.taglist_squares_unsel    = theme_path .. "taglist/clone/square_unsel.png"

-- alternate override
-- theme.taglist_squares_sel   = theme_path .. "taglist/copycat-blackburn/square_sel.png"
-- theme.taglist_squares_unsel = theme_path .. "taglist/copycat-blackburn/square_unsel.png"
-- theme.taglist_squares_sel   = theme_path .. "taglist/copycat-zenburn/squarefz.png"
-- theme.taglist_squares_unsel = theme_path .. "taglist/copycat-zenburn/squareza.png"


-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon  = theme_path .. "misc/default/submenu.png"

theme.menu_height = 20      -- dpi(15)
theme.menu_width  = 180     -- dpi(100)
--theme.menu_context_height = 20

theme.menu_bg_normal = palette.color['bgnormal']  .. "cc"
theme.menu_bg_focus  = palette.color['red'] .. "cc"
theme.menu_fg_focus  = palette.color['fgnormal']

theme.menu_border_color = palette.color['blue'] .. "cc"
theme.menu_border_width = 1

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"
