local _M = {}

_M.color = {
    ["fgfocus"] = "#586e75",
    ["fgnormal"] = "#657b83",
    ["fgdim"] = "#93a1a1",
    ["bgfocus"] = "eee8d5",
    ["bgnormal"] = "#fdf6e3",
    ["yellow"] = "#b58900",
    ["orange"] = "#cb4b16",
    ["red"] = "#dc322f",
    ["magenta"] = "#d33682",
    ["violet"] = "#6c71c4",
    ["blue"] = "#268bd2",
    ["cyan"] = "#2aa198",
    ["green"] = "#859900",
}

return _M
