-- Standard awesome library
local awful = require("awful")

local _M = {}

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

function _M.get()
    local tags = {}

    awful.screen.connect_for_each_screen(function(s)
        -- Each screen has its own tag table.
        -- tags[s] = awful.tag(
        --     { " ", " ", " ", "󱂶 ", " ", "󰊖 ", "󰐌 ", "󰏆 ", "9" },
        --     s,
        --     RC.layouts[1]
        -- )
        tags[s] = {
            awful.tag.add(
                " ",
                {
                    layout = awful.layout.suit.floating,
                    gap_single_client = false,
                    screen = s,
                    selected = true,
                }
            ),
            awful.tag.add(
                " ",
                {
                    layout = awful.layout.suit.max,
                    gap_single_client = false,
                    screen = s,
                }
            ),
            awful.tag.add(
                " ",
                {
                    layout = awful.layout.suit.spiral.dwindle,
                    gap_single_client = true,
                    gap = 10,
                    screen = s,
                }
            ),
            awful.tag.add(
               "󱂶 ",
                {
                    layout = awful.layout.suit.tile,
                    gap_single_client = false,
                    screen = s,
                }
            ),
            awful.tag.add(
                 " ",
                {
                    layout = awful.layout.suit.max,
                    gap_single_client = false,
                    screen = s,
                }
            ),
            awful.tag.add(
                 "󰊖 ",
                {
                    layout = awful.layout.suit.max.fullscreen,
                    gap_single_client = false,
                    screen = s,
                }
            ),
            awful.tag.add(
                 "󰐌 ",
                {
                    layout = awful.layout.suit.max.fullscreen,
                    gap_single_client = false,
                    screen = s,
                }
            ),
            awful.tag.add(
                 "󰏆 ",
                {
                    layout = awful.layout.suit.max,
                    gap_single_client = false,
                    screen = s,
                }
            ),
        }
    end)

    return tags
end

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

return setmetatable({}, {
    __call = function(_, ...)
        return _M.get(...)
    end,
})
