# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' matcher-list '' 'r:|[._-]=* r:|=*' 'm:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 3
zstyle :compinstall filename "${ZDOTDIR}/.zshrc"

autoload -Uz compinit
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTSIZE=5000
SAVEHIST=5000
setopt autocd beep nomatch
bindkey -e
# End of lines configured by zsh-newuser-install

export HISTFILE="${XDG_STATE_HOME}/zsh/history"
AUTO_CD=true

setopt append_history hist_expire_dups_first hist_find_no_dups hist_ignore_dups inc_append_history share_history

export PATH="${HOME}/.cargo/bin:${PATH}"
export VISUAL=vim
export EDITOR=vim
export GOPATH="${HOME}/.local"

source $HOME/.local/state/nix/profile/share/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=245'

source $HOME/.local/state/nix/profile/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=33'
ZSH_HIGHLIGHT_STYLES[default]='fg=241'
ZSH_HIGHLIGHT_STYLES[path]='fg=64'
ZSH_HIGHLIGHT_STYLES[autodirectory]='fg=64'
ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=37'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=61'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=61'
ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=160'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-unclosed]='fg=160'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument-unclosed]='fg=160'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument-unclosed]='fg=160'
ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument-unclosed]='fg=160'
ZSH_HIGHLIGHT_STYLES[comment]='fg=245'
ZSH_HIGHLIGHT_STYLES[arg0]='fg=33'
ZSH_HIGHLIGHT_STYLES[command]='fg=33'

eval "$(starship init zsh)"
eval "$(atuin init zsh --disable-up-arrow)"

alias jpr="jupyter-lab --no-browser --port=10000 --notebook-dir=. > /dev/null 2>&1 &!"
alias qwen="llama-server -m ~/Models/qwen2.5-coder-7b-instruct-q4_k_m.gguf --port 8083 --host 0.0.0.0 --gpu-layers 28 --flash-attn > ~/.config/llama-server/qwen-coder.txt 2>&1 &!"
alias lyed="fd . ~/.local/share/lyrics | fzf --print0 | xargs --null --open-tty -I {} vim {}"
alias lypr="fd . ~/.local/share/lyrics | fzf | xargs -I {} bat {}"
alias rocker="docker run --rm --name rstudio --cpus="8.0" -dt -e PASSWORD=rstudio --memory="8g" -p 127.0.0.1:8787:8787 -v ~/dockershare/R:/home/rstudio rocker/geospatial"
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

countdown
