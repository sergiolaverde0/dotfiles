local null_ls_status_ok, null_ls = pcall(require, "null-ls")
if not null_ls_status_ok then
    return
end

local diagnostics = null_ls.builtins.diagnostics
local formatting = null_ls.builtins.formatting

null_ls.setup({
    sources = {
        diagnostics.cppcheck,
        diagnostics.hadolint,
        diagnostics.selene,
        diagnostics.tidy,
        formatting.prettier,
        formatting.stylua,
        formatting.tidy,
    },
})
