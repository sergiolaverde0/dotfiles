-- [[ Setting options ]]
-- See `:help vim.o`and :help options

vim.o.foldlevel = 99

vim.opt.bg = "light"
vim.opt.breakindent = true
vim.opt.clipboard = "unnamed"
vim.opt.completeopt = { "menuone", "noselect" }
vim.opt.conceallevel = 1
vim.opt.cursorline = true
vim.opt.expandtab = true
vim.opt.ignorecase = true
vim.opt.linebreak = true
vim.opt.mouse = "a"
vim.opt.number = true
vim.opt.scrolloff = 4
vim.opt.shiftwidth = 4
vim.opt.showmode = false
vim.opt.signcolumn = "yes"
vim.opt.smartcase = true
vim.opt.smartindent = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.swapfile = false
vim.opt.tabstop = 4
vim.opt.termguicolors = true
vim.opt.undofile = true
vim.opt.updatetime = 250
vim.opt.wrap = true

local colorscheme = "solarized"

local status_color, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_color then
    return
end

vim.cmd("autocmd BufNewFile,BufRead *Dockerfile* setf dockerfile")
