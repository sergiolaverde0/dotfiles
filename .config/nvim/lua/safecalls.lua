local ibl_ok, ibl = pcall(require, "ibl")
if not ibl_ok then
    return
end
ibl.setup({
    indent = { char = "┊" },
})

local lualine_ok, lualine = pcall(require, "lualine")
if not lualine_ok then
    return
end
lualine.setup({
    options = {
        icons_enabled = true,
        theme = "solarized_light",
        disabled_filetypes = { "NvimTree" },
        always_divide_middle = true,
        globalstatus = false,
    },
    sections = {
        lualine_x = { "overseer" },
    },
})

-- See `:help gitsigns.txt`
local gitsigns_ok, gitsigns = pcall(require, "gitsigns")
if not gitsigns_ok then
    return
end
gitsigns.setup({
    signs = {
        add = { text = "+" },
        change = { text = "~" },
        delete = { text = "_" },
        topdelete = { text = "‾" },
        changedelete = { text = "~" },
    },
})

vim.g.rustaceanvim = {
    server = {
        settings = {
            ["rust-analyzer"] = {
                check = {
                    command = "clippy",
                    extraArgs = { "--", "-W", "clippy::all" },
                },
            },
        },
    },
    tools = {
        test_executor = "background",
    },
}

local ufo_ok, ufo = pcall(require, "ufo")
if not ufo_ok then
    return
end
ufo.setup()

local obsidian_ok, obsidian = pcall(require, "obsidian")
if not obsidian_ok then
    return
end
obsidian.setup({
    workspaces = {
        {
            name = "zettelkasten",
            path = vim.fn.expand("~/Zettelkasten"),
        },
    },
    mappings = {},
})

local hardtime_ok, hardtime = pcall(require, "hardtime")
if not hardtime_ok then
    return
end
hardtime.setup({
    max_time = 500,
    disable_mouse = false,
    disabled_keys = {
        ["<Up>"] = { "", "n" },
        ["<Down>"] = { "", "n" },
        ["<Left>"] = { "", "n" },
        ["<Right>"] = { "", "n" },
    },
})

local overseer_ok, overseer = pcall(require, "overseer")
if not overseer_ok then
    return
end
overseer.setup()

local context_ok, context = pcall(require, "nvim-treesitter-context")
if not context_ok then
    return
end
context.setup{}
