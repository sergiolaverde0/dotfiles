-- [[ Configure Telescope ]]
-- See `:help telesceope` and `:help telescope.setup()`
local status_ok, telescope = pcall(require, "telescope")
if not status_ok then
    return
end
telescope.setup({
    defaults = {
        mappings = {
            i = {
                ["<C-u>"] = false,
                ["<C-d>"] = false,
            },
        },
    },
    pickers = {
        find_files = {
            hidden = true
        },
        live_grep = {
            additional_args = {"--hidden"}
        },
    }
})

-- Enable telescope fzf native, if installed
telescope.load_extension("fzf")
