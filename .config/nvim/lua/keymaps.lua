-- [[ Basic Keymaps ]]
-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

local kmap = vim.keymap.set
local opts = { noremap = true, silent = true }

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
kmap({ "n", "v" }, "<Space>", "<Nop>", { silent = true })

-- Remap for dealing with word wrap
kmap("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
kmap("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Better window movement
kmap("n", "<C-h>", "<C-w>h", { opts[1], opts[2], desc = "Move to left split" })
kmap("n", "<C-j>", "<C-w>j", { opts[1], opts[2], desc = "move to bottom split" })
kmap("n", "<C-k>", "<C-w>k", { opts[1], opts[2], desc = "Move to top split" })
kmap("n", "<C-l>", "<C-w>l", { opts[1], opts[2], desc = "Move to right split" })
kmap("n", "<C-s>", "<C-w>s", { opts[1], opts[2], desc = "Create horizontal split" })
kmap("n", "<C-v>", "<C-w>v", { opts[1], opts[2], desc = "Create vertical split" })

kmap("n", "<C-Up>", ":resize -4<CR>", { opts[1], opts[2], desc = "Shrink vertical split" })
kmap("n", "<C-Down>", ":resize +4<CR>", { opts[1], opts[2], desc = "Expand vertical split" })
kmap(
    "n",
    "<C-Left>",
    ":vertical resize -4<CR>",
    { opts[1], opts[2], desc = "Shrink horizontal split" }
)
kmap(
    "n",
    "<C-Right>",
    ":vertical resize +4<CR>",
    { opts[1], opts[2], desc = "Expand horizontal split" }
)

-- Better indent in visual mode
kmap("v", "<", "<gv", { opts[1], opts[2], desc = "Reduce indent" })
kmap("v", ">", ">gv", { opts[1], opts[2], desc = "Increase indent" })

-- Move blocks of text up and down
kmap({ "n", "v" }, "<A-j>", ":m .+1<CR>", { opts[1], opts[2], desc = "Move block down" })
kmap({ "n", "v" }, "<A-k>", ":m .-2<CR>", { opts[1], opts[2], desc = "Move block up" })

kmap("x", "<A-j>", ":m '>+1<CR>gv-gv", { opts[1], opts[2], desc = "Move block down" })
kmap("x", "<A-k>", ":m '<-2<CR>gv-gv", { opts[1], opts[2], desc = "Move block up" })

-- Bufferline
kmap(
    "n",
    "<leader>b1",
    ":BufferLineGoToBuffer 1<CR>",
    { opts[1], opts[2], desc = "Go to buffer [1]" }
)
kmap(
    "n",
    "<leader>b2",
    ":BufferLineGoToBuffer 2<CR>",
    { opts[1], opts[2], desc = "Go to buffer [2]" }
)
kmap(
    "n",
    "<leader>b3",
    ":BufferLineGoToBuffer 3<CR>",
    { opts[1], opts[2], desc = "Go to buffer [3]" }
)
kmap(
    "n",
    "<leader>b4",
    ":BufferLineGoToBuffer 4<CR>",
    { opts[1], opts[2], desc = "Go to buffer [4]" }
)
kmap(
    "n",
    "<leader>b5",
    ":BufferLineGoToBuffer 5<CR>",
    { opts[1], opts[2], desc = "Go to buffer [5]" }
)
kmap(
    "n",
    "<leader>b6",
    ":BufferLineGoToBuffer 6<CR>",
    { opts[1], opts[2], desc = "Go to buffer [6]" }
)
kmap(
    "n",
    "<leader>b7",
    ":BufferLineGoToBuffer 7<CR>",
    { opts[1], opts[2], desc = "Go to buffer [7]" }
)
kmap(
    "n",
    "<leader>b8",
    ":BufferLineGoToBuffer 8<CR>",
    { opts[1], opts[2], desc = "Go to buffer [8]" }
)
kmap(
    "n",
    "<leader>b9",
    ":BufferLineGoToBuffer 9<CR>",
    { opts[1], opts[2], desc = "Go to buffer [9]" }
)
kmap("n", "<leader>bc", ":Bdelete<CR>", { opts[1], opts[2], desc = "[C]lose buffer" })
kmap(
    "n",
    "<leader>bd",
    ":BufferLineSortByDirectory<CR>",
    { opts[1], opts[2], desc = "Sort buffer by [D]irectory" }
)
kmap(
    "n",
    "<leader>be",
    ":BufferLineSortByExtension<CR>",
    { opts[1], opts[2], desc = "Sort buffer by [E]xtension" }
)
kmap(
    "n",
    "<leader>bn",
    ":BufferLineCycleNext<CR>",
    { opts[1], opts[2], desc = "Go to [N]ext buffer" }
)
kmap(
    "n",
    "<leader>bp",
    ":BufferLineCyclePrev<CR>",
    { opts[1], opts[2], desc = "Go to [P]revious buffer" }
)
kmap(
    "n",
    "<leader>bq",
    ":Bdelete!<CR>",
    { opts[1], opts[2], desc = "[Q]uit buffer, discard changes" }
)

-- LSP keybindings
kmap(
    "n",
    "<leader>e",
    vim.diagnostic.open_float,
    { opts[1], opts[2], desc = "[E]xpand diagnostics" }
)
kmap("n", "<leader>lh", vim.lsp.buf.hover, { opts[1], opts[2], desc = "[H]over Documentation" })
kmap("n", "<leader>lr", vim.lsp.buf.rename, { opts[1], opts[2], desc = "[R]ename" })
kmap("n", "<leader>la", vim.lsp.buf.code_action, { opts[1], opts[2], desc = "Code [A]ction" })
kmap("n", "<leader>ld", vim.lsp.buf.definition, { opts[1], opts[2], desc = "Goto [D]efinition" })
kmap(
    "n",
    "<leader>li",
    vim.lsp.buf.implementation,
    { opts[1], opts[2], desc = "Goto [I]mplementation" }
)
kmap(
    "n",
    "<leader>ls",
    ":Telescope lsp_document_symbols <CR>",
    { opts[1], opts[2], desc = "Document [S]ymbols" }
)
kmap(
    "n",
    "<leader>lw",
    ":Telescope lsp_dynamic_workspace_symbols <CR>",
    { opts[1], opts[2], desc = "[W]orkspace symbols" }
)
kmap(
    "n",
    "<Leader>lk",
    vim.lsp.buf.signature_help,
    { opts[1], opts[2], desc = "Signature Documentation" }
)
kmap(
    "n",
    "<leader>wa",
    vim.lsp.buf.add_workspace_folder,
    { opts[1], opts[2], desc = "[A]dd Folder" }
)
kmap("n", "<Leader>wd", vim.lsp.buf.declaration, { opts[1], opts[2], desc = "Goto [D]eclaration" })
kmap("n", "<leader>wl", function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end, { opts[1], opts[2], desc = "[L]ist Folders" })
kmap(
    "n",
    "<leader>wr",
    vim.lsp.buf.remove_workspace_folder,
    { opts[1], opts[2], desc = "[R]emove Folder" }
)
kmap(
    "n",
    "<leader>wt",
    vim.lsp.buf.type_definition,
    { opts[1], opts[2], desc = "[W]orkspace [T]ype Definition" }
)

-- rustaceanvim
kmap("n", "<leader>ra", ":RustLsp codeAction<CR>", { opts[1], opts[2], desc = "Code [A]ction" })
kmap("n", "<leader>rt", ":RustLsp testables<CR>", { opts[1], opts[2], desc = "Run [T]est" })
kmap(
    "n",
    "<leader>re",
    ":RustLsp explainError<CR>",
    { opts[1], opts[2], desc = "[E]xplain error" }
)
kmap("n", "<leader>rj", ":RustLsp joinLines<CR>", { opts[1], opts[2], desc = "[J]oin lines" })

-- Formatting with null-ls
kmap("n", "<Leader>lf", ":lua vim.lsp.buf.format() <CR>", { opts[1], opts[2], desc = "[F]ormat" })

-- Telescope
kmap(
    "n",
    "<Leader>sb",
    ":Telescope buffers <CR>",
    { opts[1], opts[2], desc = "[S]earch existing [B]uffers" }
)
kmap(
    "n",
    "<Leader>sd",
    ":Telescope diagnostics <CR>",
    { opts[1], opts[2], desc = "[S]earch [D]iagnostics" }
)
kmap(
    "n",
    "<Leader>sf",
    ":Telescope find_files <CR>",
    { opts[1], opts[2], desc = "[S]earch [F]iles" }
)
kmap(
    "n",
    "<Leader>sg",
    ":Telescope live_grep <CR>",
    { opts[1], opts[2], desc = "[S]earch by [G]rep" }
)
kmap(
    "n",
    "<Leader>sh",
    ":Telescope oldfiles <CR>",
    { opts[1], opts[2], desc = "[S]earch in file [H]istory" }
)
kmap(
    "n",
    "<leader>sr",
    ":Telescope lsp_references <CR>",
    { opts[1], opts[2], desc = "[S]earch [R]eferences" }
)
kmap(
    "n",
    "<leader>ss",
    ":Telescope spell_suggest <CR>",
    { opts[1], opts[2], desc = "[S]pell [S]uggestions" }
)
kmap(
    "n",
    "<Leader>sw",
    ":Telescope grep_string <CR>",
    { opts[1], opts[2], desc = "[S]earch current [W]ord" }
)
kmap("n", "<Leader>s?", ":Telescope help_tags <CR>", { opts[1], opts[2], desc = "Search help" })
kmap(
    "n",
    "<Leader>s/",
    ":Telescope current_buffer_fuzzy_find <CR>",
    { opts[1], opts[2], desc = "Fuzzy search in current buffer" }
)

-- Treesitter Diagnostic keymaps
kmap(
    "n",
    "<Leader>th",
    vim.diagnostic.goto_prev,
    { opts[1], opts[2], desc = "Go to previous ocurrence" }
)
kmap(
    "n",
    "<Leader>tl",
    vim.diagnostic.goto_next,
    { opts[1], opts[2], desc = "Go to next ocurrence" }
)

-- Toggle Treesitter playground
kmap(
    "n",
    "<Leader>tp",
    ":TSPlayground <CR> <C-w>l",
    { opts[1], opts[2], desc = "Open [P]layground" }
)

-- Files keybindings
kmap("n", "<leader>ff", ":lua MiniFiles.open()<CR>", { opts[1], opts[2], desc = "[F]ile Navigator" })

-- Overseer Tasks
kmap("n", "<leader>tv", ":OverseerToggle<CR>", { opts[1], opts[2], desc = "[V]iew tasks" })
kmap("n", "<leader>tr", ":OverseerRun<CR>", { opts[1], opts[2], desc = "[R]un task" })

-- Zettelkasten keybindings
kmap("n", "<Leader>zn", ":ObsidianNew ", { opts[1], opts[2], desc = "[N]ew note" })
kmap("n", "<Leader>zr", ":ObsidianRename ", { opts[1], opts[2], desc = "[R]ename note" })
kmap("n", "<Leader>zs", ":ObsidianQuickSwitch<CR>", { opts[1], opts[2], desc = "[S]earch note" })
kmap("n", "<Leader>zf", ":ObsidianFollowLink<CR>", { opts[1], opts[2], desc = "[F]ollow link" })
kmap(
    "n",
    "<Leader>zh",
    ":ObsidianFollowLink hsplit<CR>",
    { opts[1], opts[2], desc = "Open link in [H]orizontal split" }
)
kmap(
    "n",
    "<Leader>zv",
    ":ObsidianFollowLink vsplit<CR>",
    { opts[1], opts[2], desc = "Open link in [V]ertical split" }
)
kmap("n", "<Leader>zl", ":ObsidianLinks<CR>", { opts[1], opts[2], desc = "View [L]inks" })
kmap("v", "<Leader>zl", ":ObsidianLink<CR>", { opts[1], opts[2], desc = "Insert [L]ink" })
kmap(
    "n",
    "<Leader>zc",
    ":ObsidianToggleCheckbox<CR>",
    { opts[1], opts[2], desc = "Toggle [C]heckbox" }
)
kmap("n", "<Leader>zt", ":ObsidianTags<CR>", { opts[1], opts[2], desc = "[T]ag ocurrences" })
kmap("n", "<Leader>zo", ":ObsidianTemplate<CR>", { opts[1], opts[2], desc = "[O]pen template" })
