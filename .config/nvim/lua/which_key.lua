local status_ok, which_key = pcall(require, "which-key")
if not status_ok then
    return
end

which_key.setup({
    plugins = {
        marks = false,
        registers = false,
        presets = {
            operators = true,
            motions = false,
            text_objects = true,
            windows = false,
            nav = false,
            z = true,
            g = true,
        },
    },
    replace = {
        ["<leader>"] = "SPC",
    },
})

which_key.add({
    { "<leader>b", group = "Bufferline" },
    { "<leader>d", group = "Dial" },
    { "<leader>f", group = "Files" },
    { "<leader>l", group = "LSP" },
    { "<leader>r", group = "Rust" },
    { "<leader>s", group = "Telescope search" },
    { "<leader>t", group = "Treesitter" },
    { "<leader>v", group = "Visual" },
    { "<leader>w", group = "Workspace" },
    { "<leader>z", group = "Zettelkasten" },
})
