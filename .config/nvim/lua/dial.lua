-- Dial plugin for increments and decrements

local kmap = vim.keymap.set

local augend_ok, augend = pcall(require, "dial.augend")
if not augend_ok then
    return
end

local config_ok, config = pcall(require, "dial.config")
if not config_ok then
    return
end

local map_ok, map = pcall(require, "dial.map")
if not map_ok then
    return
end

local function manipulate(action, type)
    return function()
        map.manipulate(action, type)
    end
end

kmap("n", "<Leader>di", manipulate("increment", "normal"), { desc = "[D]ial [I]ncrement" })
kmap("n", "<Leader>dd", manipulate("decrement", "normal"), { desc = "[D]ial [D]ecrement" })
kmap("n", "<Leader>dgi", manipulate("increment", "gnormal"), { desc = "[D]ial [I]ncrement" })
kmap("n", "<Leader>dgd", manipulate("decrement", "gnormal"), { desc = "[D]ial [D]ecrement" })

kmap("v", "<Leader>di", manipulate("increment", "visual"), { desc = "[D]ial [I]ncrement" })
kmap("v", "<Leader>dd", manipulate("decrement", "visual"), { desc = "[D]ial [D]ecrement" })
kmap("v", "<Leader>dgi", manipulate("increment", "gvisual"), { desc = "[D]ial [I]ncrement" })
kmap("v", "<Leader>dgd", manipulate("decrement", "gvisual"), { desc = "[D]ial [I]ncrement" })

config.augends:register_group({
    default = {
        augend.semver.alias.semver,
        augend.date.alias["%Y/%m/%d"],
        augend.date.alias["%Y-%m-%d"],
        augend.date.alias["%H:%M"],
        augend.constant.alias.bool,
        augend.integer.alias.decimal,
    },
})
