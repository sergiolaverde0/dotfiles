-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`
local status_ok, nvim_ts = pcall(require, "nvim-treesitter.configs")
if not status_ok then
    return
end
nvim_ts.setup({
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = {
        "css",
        "go",
        "html",
        "javascript",
        "lua",
        "markdown",
        "markdown_inline",
        "python",
        "rust",
        "typescript",
        "vimdoc",
    },
    highlight = { enable = true },
    indent = { enable = true },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "<Leader>vi",
            node_incremental = "<Leader>va",
            scope_incremental = "<Leader>vs",
            node_decremental = "<Leader>vd",
        },
    },
    textobjects = {
        select = {
            enable = true,
            lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
            keymaps = {
                -- You can use the capture groups defined in textobjects.scm
                ["af"] = { query = "@function.outer", desc = "[A]round [F]unction" },
                ["if"] = { query = "@function.inner", desc = "[I]nside [F]unction" },
                ["ac"] = { query = "@class.outer", desc = "[A]round [C]lass" },
                ["ic"] = { query = "@class.inner", desc = "[I]nside [C]lass" },
                ["is"] = { query = "@scope", desc = "[I]nside [S]cope" },
            },
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                ["g{"] = { query = "@function.outer", desc = "Move to next function start" },
                ["g["] = { query = "@class.outer", desc = "Move to next class start" },
            },
            goto_next_end = {
                ["g}"] = { query = "@function.outer", desc = "Move to next function end" },
                ["g]"] = { query = "@class.outer", desc = "Move to next class end" },
            },
            goto_previous_start = {
                ["gp{"] = { query = "@function.outer", desc = "Move to previous function start" },
                ["gp["] = { query = "@class.outer", desc = "Move to previous class start" },
            },
            goto_previous_end = {
                ["gp}"] = { query = "@function.outer", desc = "Move to previous function end" },
                ["gp]"] = { query = "@class.outer", desc = "Move to previous class end" },
            },
        },
        swap = {
            enable = true,
            swap_next = {
                ["<leader>tf"] = { query = "@parameter.inner", desc = "Swap paremeter [F]orward" },
            },
            swap_previous = {
                ["<leader>tb"] = { query = "@parameter.inner", desc = "Swap parameter [B]ackward" },
            },
        },
    },
    playground = {
        enable = true,
    },
})
