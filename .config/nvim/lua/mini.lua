local mini_align_ok, align = pcall(require, "mini.align")
if not mini_align_ok then
    return
end
align.setup()

local mini_comment_ok, comment = pcall(require, "mini.comment")
if not mini_comment_ok then
    return
end
comment.setup()

local mini_files_ok, files = pcall(require, "mini.files")
if not mini_files_ok then
    return
end
files.setup()

local mini_pairs_ok, pairs = pcall(require, "mini.pairs")
if not mini_pairs_ok then
    return
end
pairs.setup()

local mini_surround_ok, surround = pcall(require, "mini.surround")
if not mini_surround_ok then
    return
end
surround.setup()

