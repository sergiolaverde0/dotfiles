local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    "nvim-lua/plenary.nvim", -- Dependency of many plugins
    "nvim-lua/popup.nvim",
    "lewis6991/gitsigns.nvim", -- Add git related info in the signs columns and popups
    "echasnovski/mini.nvim",
    -- Treesitter
    { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" }, -- Highlight, edit, and navigate code
    "nvim-treesitter/nvim-treesitter-textobjects", -- Additional textobjects for treesitter
    "nvim-treesitter/nvim-treesitter-context",
    -- LSP
    "neovim/nvim-lspconfig", -- Collection of configurations for built-in LSP client
    { "mrcjkb/rustaceanvim", ft = { "rust" } }, -- Improved tooling for Rust
    "nvimtools/none-ls.nvim",
    "doctorfree/cheatsheet.nvim", -- Cheatsheet for gitsign, telescope and more
    "monaqa/dial.nvim",
    "folke/which-key.nvim",
    -- Autocompletion
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-nvim-lsp-signature-help",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "kdheepak/cmp-latex-symbols",
    "hrsh7th/nvim-cmp",
    "saadparwaiz1/cmp_luasnip",
    "SergioRibera/cmp-dotenv",
    "ray-x/cmp-treesitter",
    { "kevinhwang91/nvim-ufo", dependencies = { "kevinhwang91/promise-async" } },
    {
        "m4xshen/hardtime.nvim",
        dependencies = { "MunifTanjim/nui.nvim", "nvim-lua/plenary.nvim" },
    }, -- Hints to improve your workflow
    { "tris203/hawtkeys.nvim", config = true },
    -- Snippets
    { "L3MON4D3/LuaSnip", build = "make install_jsregexp" },
    "rafamadriz/friendly-snippets",
    "ishan9299/nvim-solarized-lua",
    "kyazdani42/nvim-web-devicons",
    "nvim-lualine/lualine.nvim", -- Fancier statusline
    "akinsho/bufferline.nvim",
    { "mcauley-penney/visual-whitespace.nvim", config = true },
    "moll/vim-bbye",
    "lukas-reineke/indent-blankline.nvim", -- Add indentation guides even on blank lines
    {
        "epwalsh/obsidian.nvim", -- Zettelkasten plugin
        version = "*",
        ft = "markdown",
        dependencies = { "nvim-lua/plenary.nvim", "nvim-telescope/telescope.nvim" },
    },
    "stevearc/overseer.nvim", -- Task runner to compile or run files
    "mfussenegger/nvim-dap", -- Debugging Adapter protocol
    "mfussenegger/nvim-dap-python", -- Debugging Adapter protocol
    {
        "kylechui/nvim-surround",
        version = "*",
        config = function()
            require("nvim-surround").setup()
        end,
    },
    -- Fuzzy Finder Algorithm which requires local dependencies to be built. Only load if `make` is available
    {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        cond = vim.fn.executable("make") == 1,
    },
    "junegunn/vim-slash", -- Automatically clear search highlight when cursor is moved
})
