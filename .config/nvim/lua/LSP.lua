-- LSP settings.
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr) end

-- nvim-cmp supports additional completion capabilities
local cmp_ok, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not cmp_ok then
    return
end

local capabilities = cmp_nvim_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities())
capabilities.textDocument.foldingRange = {
    dynamicRegistration = false,
    lineFoldingOnly = true,
}

-- Enable the following language servers with default configs
local servers = {
    "basedpyright",
    "bashls",
    "clangd",
    "cssls",
    "dockerls",
    "gopls",
    "harper_ls",
    "html",
    "jsonls",
    "nil_ls",
    "ruff",
    "taplo",
    "texlab",
    "ts_ls",
}

for _, lsp in ipairs(servers) do
    require("lspconfig")[lsp].setup({
        on_attach = on_attach,
        capabilities = capabilities,
    })
end

-- Disable virtual text
vim.diagnostic.config({
    virtual_text = false,
    severity_sort = true,
    float = {
        border = "single",
        source = true,
    },
})

-- Example custom configuration for lua
--
-- Make runtime files discoverable to the server
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require("lspconfig").lua_ls.setup({
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT)
                version = "LuaJIT",
                -- Setup your lua path
                path = runtime_path,
            },
            diagnostics = {
                globals = { "vim" },
            },
            workspace = {
                library = vim.api.nvim_get_runtime_file("", true),
                checkThirdParty = false,
            },
            telemetry = { enable = "false" },
            format = { enable = "false" },
        },
    },
})

require("lspconfig").eslint.setup({
    on_attach = on_attach,
    capabilities = capabilities,
    root_dir = require("lspconfig").util.root_pattern(".git"),
    settings = {
        nodePath = "/home/desktop/.local/state/nix/profile/bin/",
    },
})
