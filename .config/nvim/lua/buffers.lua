local status_ok, bufferline = pcall(require, "bufferline")
if not status_ok then
    return
end

bufferline.setup({
    options = {
        numbers = "none", -- | "ordinal" | "buffer_id" | "both" | function({ ordinal, id, lower, raise }): string,
        indicator = { style = "icon", icon = "▎" },
        -- buffer_close_icon = "",
        buffer_close_icon = "",
        modified_icon = "●",
        -- close_icon = "",
        close_icon = "",
        left_trunc_marker = "",
        right_trunc_marker = "",
        max_name_length = 26,
        max_prefix_length = 10, -- prefix used when a buffer is de-duplicated
        tab_size = 26,
        diagnostics = false, -- | "nvim_lsp" | "coc",
        close_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
        right_mouse_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
        offsets = { { filetype = "NvimTree", text = "", padding = 1 } },
        separator_style = "thin", -- | "thick" | "thin" | { 'any', 'any' },
    },
})
