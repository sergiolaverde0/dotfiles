require("plugins")

-- Create necessary folder when saving a file

vim.api.nvim_create_autocmd("BufWritePre", {
    group = vim.api.nvim_create_augroup("auto_create_dir", { clear = true }),
    callback = function(ctx)
        vim.fn.mkdir(vim.fn.fnamemodify(ctx.file, ":p:h"), "p")
    end,
})

require("options")
require("keymaps")
require("dial")

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup("YankHighlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
    callback = function()
        vim.highlight.on_yank()
    end,
    group = highlight_group,
    pattern = "*",
})

-- Safely enable Plugins
require("safecalls")
require("buffers")
require("search")
require("treesitter")
require("LSP")
require("mini")
require("complete")
require("null_ls")
require("which_key")

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
