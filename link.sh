#!/bin/bash

mkdir -p "$HOME"/.local/share/mpd/playlists

ln -sf "$PWD"/.config/atuin                 "$HOME"/.config/
ln -sf "$PWD"/.config/bat                   "$HOME"/.config/
ln -sf "$PWD"/.config/bottom                "$HOME"/.config/
ln -sf "$PWD"/.config/dooit                 "$HOME"/.config/
ln -sf "$PWD"/.config/git                   "$HOME"/.config/
ln -sf "$PWD"/.config/kitty                 "$HOME"/.config/
ln -sf "$PWD"/.config/home-manager          "$HOME"/.config/
ln -sf "$PWD"/.config/lf                    "$HOME"/.config/
ln -sf "$PWD"/.config/lsd                   "$HOME"/.config/
ln -sf "$PWD"/.config/macchina              "$HOME"/.config/
ln -sf "$PWD"/.config/mangal                "$HOME"/.config/
ln -sf "$PWD"/.config/mpd                   "$HOME"/.config/
ln -sf "$PWD"/.config/mpv                   "$HOME"/.config/
ln -sf "$PWD"/.config/ncmpcpp               "$HOME"/.config/
ln -sf "$PWD"/.config/nvim                  "$HOME"/.config/
ln -sf "$PWD"/.config/starship.toml         "$HOME"/.config/
ln -sf "$PWD"/.config/zsh                   "$HOME"/.config/

ln -sf "$PWD"/.vim/autoload                 "$HOME"/.vim
ln -sf "$PWD"/.vim/vimrc                    "$HOME"/.vim/vimrc
ln -sf "$PWD"/.zshenv                       "$HOME"/
ln -sf "$PWD"/flat-clear.sh                 "$HOME"/
