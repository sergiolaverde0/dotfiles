#!/bin/bash
for f in *.bib
do
if [ -e "$f" ]; then
        sed -s "s/á/{\\\'a}/g"
        sed -s "s/é/{\\\'e}/g"
        sed -s "s/í/{\\\'i}/g"
        sed -s "s/ó/{\\\'o}/g"
        sed -s "s/ú/{\\\'u}/g"
    else
        echo "Error: $f not found"
    fi
done
